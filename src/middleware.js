import { searchMovies } from './actions';
import qs from 'qs';

export const searchPage = store => next => action => {
  if (action.type === '@@router/LOCATION_CHANGE') {
    const { router } = store.getState();
    console.log(router.location.query);

    const queryParams = qs.parse(action.payload.location.search.substr(1));

    if (queryParams.q) {
      store.dispatch(searchMovies(queryParams.q));
    }
  }

  next(action);
}