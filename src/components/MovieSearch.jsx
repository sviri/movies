import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

const MovieSearch = ({ push }) => {
  const inputRef = useRef(null);

  return <form onSubmit={(event) => {
    event.preventDefault();
    push(`/s?q=${inputRef.current.value}`);
  }}>
    <input ref={inputRef} type='text' />
    <button type='submit'>Search</button>
  </form>;
}

const mapDispatchToProps = {
  push,
};

const connector = connect(null, mapDispatchToProps);

export default connector(MovieSearch);