import React from 'react';
import { connect } from 'react-redux';
import { UiStatus } from '../types';

const Movies = ({ movies, isLoading, isError }) =>
  isError
    ? 'Please try again'
    : isLoading
      ? 'Please wait ...'
      : movies.map(movie => <div key={movie.id}>{movie.title}</div>);

const mapStateToProps = (state) => ({
  movies: Object.values(state.movies.data),
  isLoading: state.ui.status === UiStatus.LOADING,
  isError: state.ui.status === UiStatus.ERROR,
});

const connector = connect(mapStateToProps);

export default connector(Movies);