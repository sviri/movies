export const UiStatus = {
  IDLE: 0,
  LOADING: 1,
  ERROR: 2,
};