import React from 'react';
import ReactDOM from 'react-dom';
import MovieSearch from './components/MovieSearch';
import Movies from './components/Movies';
import uiSlice from './slices/uiSlice';
import moviesSlice from './slices/moviesSlice';
import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import {
  Switch,
  Route,
} from "react-router";
import { ConnectedRouter, connectRouter, routerMiddleware, push } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { searchPage } from './middleware';
import Manu from './components/Manu';

const history = createBrowserHistory();

const reducer = combineReducers({
  movies: moviesSlice.reducer,
  ui: uiSlice.reducer,
  router: connectRouter(history),
});

const store = configureStore({
  reducer,
  middleware: [
    ...getDefaultMiddleware(),
    routerMiddleware(history),
    searchPage,
  ],
});

const App = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/manu">
          <Manu />
        </Route>
        <Route path="/s">
          <MovieSearch />
          <Movies />
        </Route>
      </Switch>
    </ConnectedRouter>
  </Provider>;

ReactDOM.render(<App />, document.getElementById('root'));
