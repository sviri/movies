import uiSlice from './slices/uiSlice';
import moviesSlice from './slices/moviesSlice';
import { UiStatus } from './types';
import MoviesRepository from './repositories/MoviesRepository';

const moviesRepository = new MoviesRepository();

export const searchMovies = (query) =>
  async (dispatch) => {
    dispatch(uiSlice.actions.setStatus({
      status: UiStatus.LOADING,
    }));

    try {
      const movies = await moviesRepository.search(query);

      dispatch(moviesSlice.actions.setList({
        movies
      }));

      dispatch(uiSlice.actions.setStatus({
        status: UiStatus.IDLE,
      }));
    } catch (error) {
      dispatch(uiSlice.actions.setStatus({
        status: UiStatus.ERROR,
      }));
    }
  };