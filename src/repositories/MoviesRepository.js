const apiUrl = 'https://api.jikan.moe/v3';

export default class MoviesRepository {
  async search(query) {
    const response = await fetch(`${apiUrl}/search/anime?q=${query}`);
    const { results = [] } = await response.json();

    MoviesRepository.throwIfResponseError(response);

    return results.map(MoviesRepository.normalizeResult);
  }

  async get(id) {
    const response = await fetch(`${apiUrl}/anime/${id}`);
    const result = await response.json();

    MoviesRepository.throwIfResponseError(response);

    return MoviesRepository.normalizeResult(result);
  }

  static throwIfResponseError(response) {
    if (response.status >= 400) {
      throw new Error(`${response.statusText} (${response.status})`);
    }
  }

  static normalizeResult(rawResult) {
    return {
      id: rawResult.mal_id,
      url: rawResult.url,
      imageUrl: rawResult.image_url,
      title: rawResult.title,
      titleJapanese: rawResult.title_japanese,
      synopsis: rawResult.synopsis,
      type: rawResult.type,
      score: rawResult.score || '?',
      scoredBy: rawResult.scored_by || '?',
      duration: rawResult.duration || '?',
      aired: (rawResult.aired && rawResult.aired.string) || '?',
      rating: rawResult.rating || '?',
      episodesCount: rawResult.episodes || '?',
      genres: (rawResult.genres || []).map(({ name }) => name),
    };
  }
}