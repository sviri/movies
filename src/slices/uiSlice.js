import { createSlice } from '@reduxjs/toolkit';
import { UiStatus } from '../types';

const initialState = {
  status: UiStatus.IDLE,
};

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    setStatus(state, action) {
      state.status = action.payload.status;
    },
  },
});

export default uiSlice;