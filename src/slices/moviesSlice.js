import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: {
    'naruto': {
      id: 'naruto'
    },
    'onepiece': {
      id: 'onepiece',
    },
  }
};

const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {
    setList(state, action) {
      state.data = {};

      action.payload.movies.forEach(movie => {
        state.data[movie.id] = movie;
      });
    },
  },
});

export default moviesSlice;